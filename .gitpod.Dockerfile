FROM gitpod/workspace-base

RUN curl -fsSL https://apt.llvm.org/llvm-snapshot.gpg.key | sudo gpg --dearmor -o /usr/share/keyrings/llvm-archive-keyring.gpg \
    && echo "deb [signed-by=/usr/share/keyrings/llvm-archive-keyring.gpg] http://apt.llvm.org/focal/ \
    llvm-toolchain-focal main" | sudo tee /etc/apt/sources.list.d/llvm.list > /dev/null \
    && sudo apt update \
    && sudo install-packages clang-15 clang-tools-15 clang-tidy-15 clangd-15 lldb-15 lld-15 ninja-build python3\
    && curl -L https://chrome-infra-packages.appspot.com/dl/gn/gn/linux-amd64/+/latest > /tmp/gn-linux-amd64.zip \
    && unzip -p /tmp/gn-linux-amd64.zip gn | sudo tee /usr/bin/gn > /dev/null \
    && sudo chmod a+rx /usr/bin/gn \
    && rm /tmp/gn-linux-amd64.zip

USER gitpod
