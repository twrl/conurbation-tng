#include "types.hh"

extern "C" auto _start() -> void {

    asm volatile ("cli; hlt; ");

}
